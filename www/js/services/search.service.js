/**
 * Created by icastilho on 1/25/16.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Search
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('vegetariando')
      .factory('Search', ['esFactory', Search]);

   function Search(esFactory) {
      return esFactory({
         //host: 'http://paas:c238fb6ed52bbc4166cc57dbfe12d4ec@dori-us-east-1.searchly.com',
         host: 'http://localhost:9200',
         //host: "http://179049f0.ngrok.io",
         log: 'trace'
      });

   }

})();