angular.module('vegetariando.controllers', ['elasticsearch'])

   .controller('DashCtrl', function($scope,$rootScope, Search) {
      $scope.$on('$ionicView.enter', function() {
         $scope.cleanSearch();
      });

      $scope.data = { q: ''};
      $scope.search = function() {
         console.log('search q:', $scope.data.q);
         if($scope.data.q.length > 2) {
            Search.search({
               index: 'recipes',
               body: ejs.Request().query(ejs.CommonTermsQuery('title', $scope.data.q))
            }).then(function (body) {
               $scope.results = body.hits.hits;
               console.trace($scope.results);
            }, function (error) {
               console.error(error.message);
            });
         }
      };

      $scope.cleanSearch = function(){
         console.log('cleaning...');
         $scope.data = { q: ''};
         $scope.results = '';
      };

   })

   .controller('CategoryCtrl', function($scope, Search) {
      $scope.$on('$ionicView.enter', function() {
         $scope.results = '';
      });

      $scope.category = function(q){
         console.log('category:', q);
            Search.search({
               index: 'recipes',
               body: ejs.Request().query(ejs.QueryStringQuery('url:'+q))
            }).then(function (body) {
               $scope.results = body.hits.hits;
            }, function (error) {
               console.error(error.message);
            });
      };
   })
   .controller('RecipeCtrl', function($scope, $stateParams) {
      $scope.$on('$ionicView.enter', function() {
         console.log($stateParams.recipe);
         if($stateParams.recipe) {
            $scope.recipe = {
               title: $stateParams.recipe.title,
               url: $stateParams.recipe.url,
               site_name: "Presunto Vegetariano",
               postAsHtml: $stateParams.recipe.postAsHtml.replace("Relacionado", "")
            };
         }

      });
   })
   .controller('AboutCtrl', function($scope) {
   });
