// Vegetariando App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'vegetariando' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'vegetariando.services' is found in services.js
// 'vegetariando.controllers' is found in controllers.js
angular.module('vegetariando', ['ionic', 'vegetariando.controllers', 'vegetariando.services'])
   .constant('$ionicLoadingConfig', {
      template: '<ion-spinner icon="ripple"  class="spinner-royal" ></ion-spinner> ',
      noBackdrop: false
   })

   .factory('httpInterceptor', function ($injector, $rootScope) {
      return {
         'request': function (config) {
            var $ionicLoading = $injector.get("$ionicLoading");

            if (!$rootScope.blockLoading)
               $rootScope.loading = $ionicLoading.show();

            return config || $q.when(config);
         },

         'response': function (response) {
            $rootScope.loading && $rootScope.loading.hide();
            return response || $q.when(response);
         }
      };
   })

   .run(function($ionicPlatform) {
      $ionicPlatform.ready(function() {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

         }
         if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
         }
      });

      /*$rootScope.$on('loading:show', function() {
         $ionicLoading.show()
      })

      $rootScope.$on('loading:hide', function() {
         $ionicLoading.hide()
      })*/
   })

   .config(function($httpProvider, $stateProvider, $urlRouterProvider) {
      $httpProvider.interceptors.push('httpInterceptor');
      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

         .state('veg', {
            url: '/veg',
            abstract: true,
            templateUrl: 'templates/menu.html'
         })
         .state('veg.dash', {
            url: '/dash',
            views: {
               'content': {
                  templateUrl: 'templates/dash.html',
                  controller: 'DashCtrl'
               }
            }
         })
         .state('veg.recipe', {
            url: '/recipe',
            params:{
               recipe:''
            },
            views: {
               'content': {
                  templateUrl: 'templates/recipe.html',
                  controller: 'RecipeCtrl'
               }
            }
         })
         .state('veg.category', {
            url: '/category',
            views: {
               'content': {
                  templateUrl: 'templates/category.html',
                  controller: 'CategoryCtrl'
               }
            }
         })
         .state('veg.about', {
            url: '/about',
            views: {
               'content': {
                  templateUrl: 'templates/about.html',
                  controller: 'AboutCtrl'
               }
            }
         });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/veg/dash');

   });
